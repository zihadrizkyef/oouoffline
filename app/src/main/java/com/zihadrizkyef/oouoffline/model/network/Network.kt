package com.zihadrizkyef.oouoffline.model.network

import android.content.Context
import android.os.Handler
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.thedeanda.lorem.LoremIpsum
import com.zihadrizkyef.oouoffline.model.pojo.ChatRoom
import com.zihadrizkyef.oouoffline.model.pojo.LoginResponse
import java.util.*

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 22/12/18.
 */
class Network(private val context: Context) {
    private val handler = Handler()
    private val random = Random()
    private val gson = Gson()

    private fun <T> process(act: () -> Unit, listener: ResponseListener<T>) {
        handler.postDelayed({
            if (random.nextInt(10) > 0) {
                act()
            } else {
                val throwable = Throwable("No Internet Connection")
                listener.onFailure(throwable)
            }
        }, 500 + random.nextInt(3000).toLong())
    }

    fun login(username: String, password: String, listener: ResponseListener<LoginResponse>) {
        process({
            if (username == "zihad" && password == "bismillah") {
                listener.onSuccess(LoginResponse(true, ""))
            } else {
                listener.onSuccess(LoginResponse(false, "Username atau password salah"))
            }
        }, listener)
    }

    fun getRoomList(listener: ResponseListener<List<ChatRoom>>) {
        process({
            val inputStream = context.assets.open("room_list.json")
            val text = inputStream.readBytes().toString(Charsets.UTF_8)
            inputStream.close()

            val type = object : TypeToken<ArrayList<ChatRoom>>() {}.type
            val chatRooms: ArrayList<ChatRoom> = gson.fromJson(text, type)
            val lorem = LoremIpsum(2642642L)
            for (i in 0 until 30) {
                val chatRoom = ChatRoom(
                    chatRooms.size,
                    "",
                    "0",
                    "2013-02-25 09:45:23",
                    lorem.getWords(15),
                    lorem.name,
                    0
                )
                chatRooms.add(chatRoom)
            }
            listener.onSuccess(chatRooms)
        }, listener)
    }
}