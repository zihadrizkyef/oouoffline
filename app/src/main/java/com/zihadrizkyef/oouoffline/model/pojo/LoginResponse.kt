package com.zihadrizkyef.oouoffline.model.pojo

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 22/12/18.
 */
class LoginResponse(
    val loginSuccess: Boolean,
    val message: String
)