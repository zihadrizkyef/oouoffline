package com.zihadrizkyef.oouoffline.model.sharedpref

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 22/12/18.
 */
class SPFKeys {
    companion object {
        const val NAME = "oou-offline-46408624642"

        const val SPLASH = "splash"
        const val LOGIN = "login"
    }
}