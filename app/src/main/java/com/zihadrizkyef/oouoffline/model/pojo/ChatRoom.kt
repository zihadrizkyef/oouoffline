package com.zihadrizkyef.oouoffline.model.pojo

import com.google.gson.annotations.SerializedName

data class ChatRoom(
    @SerializedName("id") val id: Int,
    @SerializedName("image_url") val imageUrl: String,
    @SerializedName("is_group") val isGroup: String,
    @SerializedName("last_activity") val lastActivity: String,
    @SerializedName("message") val message: String,
    @SerializedName("name") val name: String,
    @SerializedName("unreaded_message") var unreadedMessage: Int
)