package com.zihadrizkyef.oouoffline.model.network

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 22/12/18.
 */
class Response<T>(
    val success: Boolean,
    val response: T
)