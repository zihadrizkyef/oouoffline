package com.zihadrizkyef.oouoffline.model.pojo

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 11/01/19.
 */
class Status(
    val text: String,
    val image: String,
    val time: String,
    val reader: String
)