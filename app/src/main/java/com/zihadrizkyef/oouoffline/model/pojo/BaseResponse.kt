package com.zihadrizkyef.oouoffline.model.pojo

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 22/12/18.
 */
class BaseResponse(
    val success: Boolean,
    val message: String
)