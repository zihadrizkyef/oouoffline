package com.zihadrizkyef.oouoffline.model.sharedpref

import android.content.Context

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 22/12/18.
 */
class SharedPref(context: Context) {
    private val INSTANCE by lazy {
        context.getSharedPreferences(SPFKeys.NAME, Context.MODE_PRIVATE)
    }

    var haveShowSplash: Boolean
        get() = INSTANCE.getBoolean(SPFKeys.SPLASH, false)
        set(value) {
            INSTANCE.edit().putBoolean(SPFKeys.SPLASH, value).apply()
        }

    var loggedIn: Boolean
        get() = INSTANCE.getBoolean(SPFKeys.LOGIN, false)
        set(value) {
            INSTANCE.edit().putBoolean(SPFKeys.LOGIN, value).apply()
        }
}