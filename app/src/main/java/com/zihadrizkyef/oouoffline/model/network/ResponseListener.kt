package com.zihadrizkyef.oouoffline.model.network

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 22/12/18.
 */
interface ResponseListener<T> {
    fun onSuccess(data: T)
    fun onFailure(throwable: Throwable)
}