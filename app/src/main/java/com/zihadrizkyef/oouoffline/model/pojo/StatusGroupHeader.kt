package com.zihadrizkyef.oouoffline.model.pojo

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 11/01/19.
 */
class StatusGroupHeader(
    val text: String
)