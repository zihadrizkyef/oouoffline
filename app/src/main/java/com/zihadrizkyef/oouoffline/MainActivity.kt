package com.zihadrizkyef.oouoffline

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.tab_text_with_badge.view.*
import zihadrizkyef.com.muadzbinjabal.adapter.CustomPagerAdapter

class MainActivity : AppCompatActivity() {
    private lateinit var pageList: ArrayList<ViewGroup>
    private lateinit var pagerAdapter: CustomPagerAdapter

    private lateinit var cameraView: CameraView
    private lateinit var chatRoomListView: ChatRoomListView
    private lateinit var statusListView: StatusListView
    private lateinit var callListView: CallListView

    private lateinit var customTabCameraView: View
    private lateinit var customTabChatView: View
    private lateinit var customTabStatusView: View
    private lateinit var customTabCallView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cameraView = CameraView(this)
        chatRoomListView = ChatRoomListView(this)
        statusListView = StatusListView(this)
        callListView = CallListView(this)

        pageList = arrayListOf(
            cameraView,
            chatRoomListView,
            statusListView,
            callListView
        )
        pagerAdapter = CustomPagerAdapter(this, pageList)
        viewPager.adapter = pagerAdapter

        tabLayout.setupWithViewPager(viewPager)
        viewPager.setCurrentItem(1, false)

        customTabCameraView = (LayoutInflater.from(this).inflate(
            R.layout.tab_icon,
            null
        ) as ImageView).apply { setImageResource(R.drawable.ic_tab_camera) }
        customTabChatView = LayoutInflater.from(this).inflate(R.layout.tab_text_with_badge, null)
            .apply { tvText.text = "Chats" }
        customTabStatusView = LayoutInflater.from(this).inflate(R.layout.tab_text_with_badge, null)
            .apply { tvText.text = "Status" }
        customTabCallView = LayoutInflater.from(this).inflate(R.layout.tab_text_with_badge, null)
            .apply { tvText.text = "Calls" }

        tabLayout.getTabAt(0)!!.customView = customTabCameraView
        tabLayout.setTabWidthAsWrapContent(0)
        tabLayout.getTabAt(1)!!.customView = customTabChatView
        tabLayout.getTabAt(2)!!.customView = customTabStatusView
        tabLayout.getTabAt(3)!!.customView = customTabCallView
    }

    private fun TabLayout.setTabWidthAsWrapContent(tabPosition: Int) {
        val layout = (this.getChildAt(0) as LinearLayout).getChildAt(tabPosition) as LinearLayout
        val layoutParams = layout.layoutParams as LinearLayout.LayoutParams
        layoutParams.weight = 0f
        layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
        layout.layoutParams = layoutParams
    }

    fun setChatTabBadge(count: Int) {
        if (count > 0) {
            customTabChatView.tvBadge.visibility = View.VISIBLE
            customTabChatView.tvBadge.text = count.toString()
        } else {
            customTabChatView.tvBadge.visibility = View.GONE
        }
    }
}
