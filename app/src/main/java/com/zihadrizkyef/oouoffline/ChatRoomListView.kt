package com.zihadrizkyef.oouoffline

import android.content.Context
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.LayoutInflater
import com.zihadrizkyef.oouoffline.adapter.AdapterChatRoomList
import com.zihadrizkyef.oouoffline.model.network.Network
import com.zihadrizkyef.oouoffline.model.network.ResponseListener
import com.zihadrizkyef.oouoffline.model.pojo.ChatRoom
import kotlinx.android.synthetic.main.view_chat_room_list.view.*

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 04/01/19.
 */
class ChatRoomListView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayoutCompat(context, attrs, defStyleAttr) {

    val roomList = arrayListOf<ChatRoom>()
    val adapter = AdapterChatRoomList(context, roomList)

    init {
        LayoutInflater.from(context).inflate(R.layout.view_chat_room_list, this, true)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
        adapter.onItemClickListener = {
            val unreadCount = roomList.sumBy { chatRoom -> chatRoom.unreadedMessage }
            (context as MainActivity).setChatTabBadge(unreadCount)
        }

        getRoomList()
    }

    private fun getRoomList() {
        Network(context).getRoomList(object : ResponseListener<List<ChatRoom>> {
            override fun onSuccess(data: List<ChatRoom>) {
                roomList.addAll(data)
                adapter.notifyDataSetChanged()

                val unreadCount = roomList.sumBy { it.unreadedMessage }
                (context as MainActivity).setChatTabBadge(unreadCount)
            }

            override fun onFailure(throwable: Throwable) {
                Snackbar.make(coordinatorLayout, "Tidak ada internet", Snackbar.LENGTH_LONG)
                    .setAction("Coba Lagi", { getRoomList() }).show()
                throwable.printStackTrace()
            }
        })
    }
}