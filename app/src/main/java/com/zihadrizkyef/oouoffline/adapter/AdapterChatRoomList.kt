package com.zihadrizkyef.oouoffline.adapter

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.signature.ObjectKey
import com.zihadrizkyef.oouoffline.R
import com.zihadrizkyef.oouoffline.model.pojo.ChatRoom
import com.zihadrizkyef.oouoffline.util.GlideApp
import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 16/07/17.
 */

class AdapterChatRoomList(private val context: Context, private var chatRooms: List<ChatRoom>) :
    RecyclerView.Adapter<ViewHolderChatRoomList>() {
    var onItemClickListener: (pos: Int) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderChatRoomList {
        val view = LayoutInflater.from(context).inflate(R.layout.list_chat_room, parent, false)
        return ViewHolderChatRoomList(this, view)
    }

    override fun onBindViewHolder(holder: ViewHolderChatRoomList, position: Int) {
        val chatRoom = chatRooms[position]

        holder.tvName.text = chatRoom.name

        holder.tvText.text = try {
            URLDecoder.decode(chatRoom.message, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
            chatRoom.message
        }

        val currentDate = Date().time
        val simpleDateFormat = SimpleDateFormat("yyyy-M-d hh:mm:ss")
        val date = simpleDateFormat.parse(chatRoom.lastActivity)
        val differentInMs = currentDate - date.time
        val differentInDays = TimeUnit.DAYS.convert(differentInMs, TimeUnit.MILLISECONDS).toInt()
        holder.tvTime.text = when {
            differentInDays == 0 -> {
                val sdf = SimpleDateFormat("hh:mm")
                sdf.format(date)
            }
            differentInDays == 1 -> "Yesterday"
            differentInDays > 1 -> {
                val sdf = SimpleDateFormat("d/M/yy")
                sdf.format(date)
            }
            else -> ""
        }

        if (chatRoom.unreadedMessage > 0) {
            holder.tvBadge.visibility = View.VISIBLE
            holder.tvBadge.text = chatRoom.unreadedMessage.toString()
            holder.tvTime.setTextColor(ContextCompat.getColor(context, R.color.colorChatRoomTimeAccent))
        } else {
            holder.tvBadge.visibility = View.GONE
            holder.tvTime.setTextColor(ContextCompat.getColor(context, R.color.colorChatRoomTimeNormal))
        }

        if (chatRoom.imageUrl.isNotBlank()) {
            GlideApp.with(context)
                .load("file:///android_asset/photo/" + chatRoom.imageUrl)
                .error(R.drawable.ic_person)
                .into(holder.ivPhoto)
        } else {
            GlideApp.with(context)
                .load("https://picsum.photos/200/?image=" + (100 + chatRoom.id))
                .signature(ObjectKey(chatRoom.id))
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .error(R.drawable.ic_person)
                .into(holder.ivPhoto)
        }
    }

    override fun getItemCount(): Int {
        return chatRooms.size
    }

    fun onItemClick(position: Int) {
        chatRooms[position].unreadedMessage = 0
        notifyItemChanged(position)

        onItemClickListener(position)
    }
}

class ViewHolderChatRoomList(adapter: AdapterChatRoomList, itemView: View) : RecyclerView.ViewHolder(itemView) {
    var constraintLayout: ConstraintLayout = itemView.findViewById(R.id.constraintLayout)
    var ivPhoto: ImageView = itemView.findViewById(R.id.ivImage)
    var tvName: TextView = itemView.findViewById(R.id.tvName)
    var tvText: TextView = itemView.findViewById(R.id.tvContent)
    val tvTime: TextView = itemView.findViewById(R.id.tvTime)
    var tvBadge: TextView = itemView.findViewById(R.id.tvBadge)

    init {
        constraintLayout.setOnClickListener {
            adapter.onItemClick(adapterPosition)
        }
    }
}
