package com.zihadrizkyef.oouoffline.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.zihadrizkyef.oouoffline.R
import com.zihadrizkyef.oouoffline.model.pojo.PersonStatus
import com.zihadrizkyef.oouoffline.model.pojo.StatusGroupHeader
import com.zihadrizkyef.oouoffline.util.GlideApp
import kotlinx.android.synthetic.main.list_status_group_title.view.*
import kotlinx.android.synthetic.main.list_status_my_status.view.*

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 11/01/19.
 */
class AdapterStatusList(private val context: Context, private val datas: List<Any>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val TYPE_MY_STATUS = 0
    private val TYPE_GROUP_HEADER = 1
    private val TYPE_OTHER_STATUS = 2

    override fun getItemViewType(position: Int): Int {
        return if (datas[position] is PersonStatus) {
            if ((datas[position] as PersonStatus).name == "Zihad Rizky Edwin Fikri") {
                TYPE_MY_STATUS
            } else {
                TYPE_OTHER_STATUS
            }
        } else {
            TYPE_GROUP_HEADER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_MY_STATUS -> {
                val v = LayoutInflater.from(context)
                    .inflate(R.layout.list_status_my_status, parent, false)
                ViewHolderMyStatus(v)
            }
            TYPE_OTHER_STATUS -> {
                val v = LayoutInflater.from(context)
                    .inflate(R.layout.list_status_other_status, parent, false)
                ViewHolderOtherStatus(v)
            }
            else -> {
                val v = LayoutInflater.from(context)
                    .inflate(R.layout.list_status_group_title, parent, false)
                ViewHolderGroupTitle(v)
            }
        }
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when (viewHolder.itemViewType) {
            TYPE_MY_STATUS -> {
                val holder = viewHolder as ViewHolderMyStatus
                val data = datas[position] as PersonStatus
                val statuses = data.statuses

                GlideApp.with(context)
                    .load("file:///android_asset/photo/" + statuses[statuses.lastIndex].image)
                    .into(holder.ivPhoto)
                holder.tvTime.text = statuses[statuses.lastIndex].time
            }

            TYPE_OTHER_STATUS -> {
                val holder = viewHolder as ViewHolderOtherStatus
                val data = datas[position] as PersonStatus
                val statuses = data.statuses

                GlideApp.with(context)
                    .load("file:///android_asset/photo/" + statuses[statuses.lastIndex].image)
                    .into(holder.ivPhoto)
                holder.tvTime.text = statuses[statuses.lastIndex].time
            }

            TYPE_GROUP_HEADER -> {
                val holder = viewHolder as ViewHolderGroupTitle
                val data = datas[position] as StatusGroupHeader

                holder.tvText.text = data.text
            }
        }
    }
}

class ViewHolderMyStatus(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val constraintLayout = itemView.constraintLayout!!
    val ivPhoto = itemView.ivImage!!
    val tvTime = itemView.tvTime!!
    val btMore = itemView.btMore!!
}

class ViewHolderGroupTitle(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tvText = itemView.tvText!!
}

class ViewHolderOtherStatus(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val constraintLayout = itemView.constraintLayout!!
    val ivPhoto = itemView.ivImage!!
    val tvTime = itemView.tvTime!!
    val btMore = itemView.btMore!!
}