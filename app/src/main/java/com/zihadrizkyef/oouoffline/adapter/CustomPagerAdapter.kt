package zihadrizkyef.com.muadzbinjabal.adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup


/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 14/11/17.
 */
class CustomPagerAdapter(private val context: Context, private val views: ArrayList<ViewGroup>) : PagerAdapter() {

    var title = arrayListOf<String?>()

    override fun getCount(): Int = views.size

    override fun instantiateItem(container: ViewGroup, position: Int): View {
        container.addView(views[position])

        return views[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position < title.size) {
            title[position]
        } else {
            ""
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view === `object`
}