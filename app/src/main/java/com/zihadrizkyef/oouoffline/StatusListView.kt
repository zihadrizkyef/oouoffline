package com.zihadrizkyef.oouoffline

import android.content.Context
import android.support.v7.widget.LinearLayoutCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.AttributeSet
import android.view.LayoutInflater
import com.zihadrizkyef.oouoffline.adapter.AdapterStatusList
import com.zihadrizkyef.oouoffline.model.network.Network
import kotlinx.android.synthetic.main.view_status_list.view.*

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 04/01/19.
 */
class StatusListView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayoutCompat(context, attrs, defStyleAttr) {
    private val datas = arrayListOf<Any>()
    private val adapter = AdapterStatusList(context, datas)

    init {
        LayoutInflater.from(context).inflate(R.layout.view_status_list, this, true)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        getStatusList()
    }

    private fun getStatusList() {
        Network(context)
    }
}