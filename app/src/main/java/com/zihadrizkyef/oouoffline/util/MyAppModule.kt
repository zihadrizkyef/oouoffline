package com.zihadrizkyef.oouoffline.util

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule


/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 07/01/19.
 */

@GlideModule
class MyAppModule : AppGlideModule()