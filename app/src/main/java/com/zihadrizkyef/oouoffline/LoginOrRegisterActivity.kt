package com.zihadrizkyef.oouoffline

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.LinearLayout
import com.zihadrizkyef.oouoffline.model.network.Network
import com.zihadrizkyef.oouoffline.model.network.ResponseListener
import com.zihadrizkyef.oouoffline.model.pojo.LoginResponse
import com.zihadrizkyef.oouoffline.model.sharedpref.SharedPref
import kotlinx.android.synthetic.main.activity_login.*


class LoginOrRegisterActivity : AppCompatActivity() {
    private val ACTION_LOGIN = 0
    private val ACTION_REGISTER = 1

    private var action = ACTION_LOGIN
    private lateinit var spanLogin: SpannableString
    private lateinit var spanSignUp: SpannableString

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setupSpannableLogin()
        setupSpannableSignUp()
        tvSignUpOrLogin.movementMethod = LinkMovementMethod.getInstance()
        tvSignUpOrLogin.text = spanSignUp

        btLoginOrRegister.setOnClickListener {
            hideKeyboard()
            if (action == ACTION_REGISTER) {
                Snackbar.make(coordinatorLayout, "Maaf, fitur belum tersedia", Snackbar.LENGTH_SHORT).show()
            } else {
                if (isFormValid()) {
                    login()
                } else {
                    Snackbar.make(coordinatorLayout, "Mohon isi semua data", Snackbar.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun setupSpannableSignUp() {
        val longStr = getString(R.string.don_t_have_account_sign_up)
        val shortStr = getString(R.string.sign_up)
        spanSignUp = SpannableString(longStr)
        spanSignUp.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                tvSignUpOrLogin.text = spanLogin
                btLoginOrRegister.text = getString(R.string.sign_up)
                etName.requestFocus()
                action = ACTION_REGISTER

                val height = dpToPx(56)
                val params = flName.layoutParams as LinearLayout.LayoutParams
                tilName.animate()
                    .translationY(0F)
                    .setUpdateListener { listener ->
                        params.height = (height * (listener.animatedValue as Float)).toInt()
                        flName.layoutParams = params
                    }.start()
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.color = ContextCompat.getColor(this@LoginOrRegisterActivity, android.R.color.white)
                ds.isUnderlineText = true
            }
        }, longStr.indexOf(shortStr), longStr.indexOf(shortStr) + shortStr.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    }

    private fun setupSpannableLogin() {
        val longStr = getString(R.string.already_have_account_login_now)
        val shortStr = getString(R.string.login)
        spanLogin = SpannableString(longStr)
        spanLogin.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View) {
                tvSignUpOrLogin.text = spanSignUp
                btLoginOrRegister.text = getString(R.string.login)
                etUsername.requestFocus()
                action = ACTION_LOGIN

                val height = flName.height.toFloat()
                val params = flName.layoutParams as LinearLayout.LayoutParams
                tilName.animate()
                    .translationY(-tilName.height.toFloat())
                    .setUpdateListener { listener ->
                        params.height = (height - (height * (listener.animatedValue as Float))).toInt()
                        flName.layoutParams = params
                    }.start()
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.color = ContextCompat.getColor(this@LoginOrRegisterActivity, android.R.color.white)
                ds.isUnderlineText = true
            }
        }, longStr.indexOf(shortStr), longStr.indexOf(shortStr) + shortStr.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        tvSignUpOrLogin.movementMethod = LinkMovementMethod.getInstance()
        tvSignUpOrLogin.text = spanLogin
    }

    private fun isFormValid(): Boolean {
        if (action == ACTION_LOGIN) {
            return etUsername.text.toString().isNotBlank() && etPassword.text.toString().isNotBlank()
        } else if (action == ACTION_REGISTER) {
            return etName.text.toString().isNotBlank() && etUsername.text.toString().isNotBlank() && etPassword.text.toString().isNotBlank()
        }
        return false
    }

    private fun login() {
        showProgressBar()

        Network(this).login(
            etUsername.text.toString(),
            etPassword.text.toString(),
            object : ResponseListener<LoginResponse> {
                override fun onSuccess(data: LoginResponse) {
                    hideProgressBar()

                    if (data.loginSuccess) {
                        SharedPref(this@LoginOrRegisterActivity).loggedIn = true

                        finish()
                        startActivity(Intent(this@LoginOrRegisterActivity, MainActivity::class.java))
                    } else {
                        Snackbar.make(coordinatorLayout, data.message, Snackbar.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(throwable: Throwable) {
                    hideProgressBar()

                    Snackbar.make(coordinatorLayout, "Tidak ada internet", Snackbar.LENGTH_LONG)
                        .setAction("Coba Lagi", { login() }).show()
                    throwable.printStackTrace()
                }
            }
        )
    }

    private fun hideKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = currentFocus

        if (view == null) {
            view = View(this)
        }

        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun showProgressBar() {
        progressBar.animate().scaleX(1F).scaleY(1F).start()
        btLoginOrRegister.animate().scaleX(0F).scaleY(0F).start()
    }

    private fun hideProgressBar() {
        progressBar.animate().scaleX(0F).scaleY(0F).start()
        btLoginOrRegister.animate().scaleX(1F).scaleY(1F).start()
    }

    fun dpToPx(dp: Int): Int {
        return (dp * Resources.getSystem().displayMetrics.density).toInt()
    }
}
