package com.zihadrizkyef.oouoffline

import android.content.Context
import android.support.v7.widget.LinearLayoutCompat
import android.util.AttributeSet

/**
 * بِسْمِ اللهِ الرَّحْمٰنِ الرَّحِيْمِ
 * Created by zihadrizkyef on 04/01/19.
 */
class CallListView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayoutCompat(context, attrs, defStyleAttr)