package com.zihadrizkyef.oouoffline

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.zihadrizkyef.oouoffline.model.sharedpref.SharedPref

class SplashActivity : AppCompatActivity() {
    val handler = Handler()
    val runnable = Runnable {
        SharedPref(this).haveShowSplash = true

        if (SharedPref(this).loggedIn) {
            startActivity(Intent(this, MainActivity::class.java))
        } else {
            startActivity(Intent(this, LoginOrRegisterActivity::class.java))
        }
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if (SharedPref(this).haveShowSplash) {
            if (SharedPref(this).loggedIn) {
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                startActivity(Intent(this, LoginOrRegisterActivity::class.java))
            }
            finish()
        }
    }

    override fun onPause() {
        super.onPause()

        handler.removeCallbacks(runnable)
    }

    override fun onResume() {
        super.onResume()

        handler.postDelayed(runnable, 2000)
    }
}
